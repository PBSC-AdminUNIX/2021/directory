# Manual de instalacion de cliente de ldap

### El presente documento explica la forma en que se instala el cliente ldap en maquinas derivadas de Debian

Primero se necesita instalar el software necesario, para ello utilizamos el siguiente comando

> `sudo apt-get install libnss-ldap libpam-ldap ldap-utils nscd -y`

En el momento de la instalacion se pregunta la ruta del servidor ldap al cual se va a conectar.
La ruta es la siguiente:

> `ldaps://directory.planbecarios15.cf:636/`
	
Posteriormente se pide ingresar la base del arbol LDAP al cual se van a conectar.
La ruta es la siguiente: **`dc=planbecarios15,dc=15`**
	
En las siguientes pantallas de configuracion se deben seleccionar los siguientes parametros

* Specify LDAP version (**selecciona 3**)
* Make local root Database admin (**selecciona Yes**)
* Does the LDAP database require login (**seleciona No**)
* Aqui deben especificar una cuenta de administrador para su servicio de LDAP
    * Specify LDAP admin account suffice (Debe tener la siguiente forma **cn=admin,dc=planbecarios15,dc=cf**)
* Aqui se debe especificar el  password para el usuario anterior
    * Specify password for LDAP admin account (Este sera el password para el usuario administrador LDAP)


Hasta este punto se ha terminado la instalacion del cliente LDAP, lo siguiente es la configuracion para que pueda funcionar correctamente

``NOTA: Los archivos que se van a editar los puede realizar con nano o vim, dependen de cual es el que mas prefieran.``

Primero editamos el siguiente archivo:

> `sudo nano /etc/nsswitch.conf`

Aqui debemos agregar ldap a las siguientes lineas

	passwd: compat systemd
	group:  compat systemd
	shadow: files

Y debe quedar de la siguiente forma

	passwd: compat systemd ldap
	group:  compat systemd ldap
	shadow: files ldap
	
Si al final de estas lineas no se encuentra la siguiente, se debe agregar

	gshadow files
	
Guardamos y cerramos este archivo. Posteriormente debemos modificar el siguiente archivo:

> `sudo nano /etc/pam.d/common-password`
	
Aqui solo debemos remover use_authtok de la siguiente linea

	-password [success=1 user_unknown=ignore default=die] pam_ldap.so use_authtok try_first_pass

Y queda de la siguiente forma

	-password [success=1 user_unknown=ignore default=die] pam_ldap.so try_first_pass

Guardamos y cerramos. Ahora editamos el siguiente archivo:

> `sudo nano /etc/pam.d/common-session`
	
Aqui agregamos lo siguiente al final del archivo

	-session optional pam_mkhomedir.so skel=/etc/skel umask=077
	
Cerramos y guardamos.

Con esto las configuraciones ya estan hechas, así que para que tengan efecto se debe reiniciar la maquina cliente.

Una vez reiniciada la maquina para verificar que funciona hacen el siguiente comando

> `getent passwd`
	
Y deberan ser capaces de ver a todos los usuarios.

Para poder cambiar las contraseñas de default de los usuarios, se debera ingresar <a href="https://www.youtube.com/watch?v=nAwzZlX806o">**aqui**</a> 

`Nota: su contrasena default se lo dara algun integrante del equipo, favor de comunicarse`
	
    
    
