# Directory

### Team members

* Galindo Perez Ivan Daniel  
* Julián José Emmanuel  
* Medina Miranda Mauricio  
* Piña Reyes Cristopher Alan  
* Varela Cruz César Alejandro  

<div align="center">
<h1> ---------------Objetivo--------------------- </h1>
</div>
El objetivo del equipo de directory es que los equipos de mail, storage y web puedan autenticar a sus usuarios a través de LDAP, aunque para esta practica, por fines de seguridad se hace uso de LDAPS, así como tambien el crear a dichos usuarios y a su vez el home de cada uno de estos y así el equipo de mail pueda acceder a ellos para almacenar los correos de cada usuario en su propio home.
![Alt text](./IMG/directory.jpg?raw=true "directory")
<div align="center">
<h1> ---------------Proceso--------------------- </h1>
</div>
El proceso para levantar nuestro servicio se realizo de la siguiente manera. Primero que nada se ha tenido que reconfigurar el gestor de paquetes para que nos haga todas las preguntas correspondientes en la configuracion del software. El comando que se uso para esto es el siguiente:
 	
 	-dpkg-reconfigure debconf

<img src="./IMG/capturas/configuracion1.png" alt="capturas" width="450"/>
<img src="./IMG/capturas/configuracion2.png" alt="capturas" width="450"/>
<br/>

`Nota: Poner una contraseña fuerte`

<p>Posteriormente a esto se procede a instalar el demonio de OPENLDAP, slapd con el siguiente comando.</p>

	-apt-get install slapd
	
<img src="./IMG/capturas/configuracion4.png" alt="capturas" width="450"/>
<img src="./IMG/capturas/configuracion5.png" alt="capturas" width="450"/>

Ya que para poder usar el administrador web se requieren de certificados SSL, los cuales han sido proporcionados por el equipo de web y para poder utilizarlos se han hecho ajustes previos en los grupos del sistema.

	-groupadd --gid 107 --system ssl-cert
	-adduser openldap ssl-cert
	-chown root:ssl-cert /etc/ssl/private/
	-chmod g+x /etc/ssl/private/

Una vez que ya se tienen los certificados y se han realizado las configuraciones previas, se procede a instalarlos en las siguientes rutas: `/etc/ssl/certs/` para el certificado y `/etc/ssl/private/` para la llave. Para esto se utilizaron los siguientes comandos:

	-install -D -o root /etc/ssl/certs/server.crt -g ssl-cert -m 644 server.crt
	-install -D -o root /etc/ssl/private/server.key -g ssl-cert -m 640 server.key
	
Con los certificados instalados se procede a modificar el archivo de configuracion de LDAP donde se le indica el puerto que utilizara para realizar la comunicacion a traves de LDAPS (en el directorio `/etc/default/slapd`).

	-SLAPD_SERVICES="ldaps://*:636/ ldap://127.0.0.1:389/ ldapi:///"

Seguido de esto se procede a configurar el archivo que se encuentra en `/etc/ldap/slapd.d/cn\=config.ldif` en donde se le agregan un par de lineas indicando donde se encuentran los certificados previamente instalados.

	-olcTLSCertificateFile: /etc/ssl/certs/server.crt
	-olcTLSCertificateKeyFile: /etc/ssl/private/server.key
	
Una vez que todas estas configuraciones se encuentran realizadas se procede a inicializar el servicio slapd y se verifica que escuche por los puertos correspondientes:
	
	-systemctl star slapd
	-netstat -natuplw | grep slapd
	
Con todas las configuraciones realizadas hasta este punto ya tenemos listo el servidor para poder ser usado pero aun no se han creado usuarios ni se ha especificado que se utilice el servicio LDAP como sistema de autenticacion por lo que procederemos a instalar un administrador basado en web el cual nos ayudara en esta parte. Para ello tendremos que instalar apache y la herramienta ldap-account-manager.

Para la instalacion de apache basta con utilizar el comando

	-apt-get install apache2
	
Y una vez instalado se deben realizar las siguientes configuraciones:

Habilitar los modulos

	-cd /etc/apache2/mods-enabled/
	-ln -s ../mods-available/ssl.conf
	-ln -s ../mods-available/ssl.load
	-ln -s ../mods-available/rewrite.load

Habilitar el virtualhost para que tenga soporte de SSL. 

	-cd ../sites-enabled/
	-ln -s ../sites-available/default-ssl
	
Editar el archivo `/etc/apache2/sites-enabled/default-ssl `.Aquí se vuelven a utilizar los certificados que se obtuvieron previamente.

<img src="./IMG/capturas/configuracion24.png" alt="capturas" width="900"/>

Con estas configuraciones hechas en apache se inicializa el servicio, para ello utilizamos el comando:
	
	-systemctl start apache2
	
Despues de instalar el servicio de apache y hacer las configuraciones correspondientes procedemos a instalar la herramienta que nos ayudara a gestionar los usuarios y grupos en LDPA.

	-apt-get install ldap-account-manager

`Web server configuration`
- [x] apache2
<br/>`Alias name`
- [x] lam
<br/>`Restart webservers now?`
- [x] Yes
<p>Por ultimo se agregan unas lineas al archivo de configuracion previamente usado y despues se reinicia el servicio de apache</p>

<img src="./IMG/capturas/configuracion25.png" alt="capturas" width="900"/>

<p>Hasta este punto se terminan todas las configuraciones dentro del servidor así como ajustes que este pueda tener y si todo ha funcionado de la mejor manera entonces ahora seremos capaces de acceder al administrador a traves de la siguiente url:</p>

-https://directory.planbecarios15.cf/lam/templates/login.php
	
<p>Antes de acceder al administrador se deben realizar unas configuraciones previas y para ello primero debemos ir al apartado que dice LAM configuration y una vez dentro ir a donde dice Edit server profile. Aqui entraremos en un area que esta dividida en varias secciones en las cuales relizaremos diversos ajustes para el correcto funcionamiento de la herramienta.</p>
<img src="./IMG/capturas/configuracion10.png" alt="capturas" width="450"/>
<img src="./IMG/capturas/configuracion26.png" alt="capturas" width="450"/>
<img src="./IMG/capturas/configuracion11.png" alt="capturas" width="750"/>
<img src="./IMG/capturas/configuracion12.png" alt="capturas" width="750"/>
<img src="./IMG/capturas/configuracion13.png" alt="capturas" width="750"/>
<img src="./IMG/capturas/configuracion14.png" alt="capturas" width="750"/>
<img src="./IMG/capturas/configuracion15.png" alt="capturas" width="750"/>
<img src="./IMG/capturas/configuracion16.png" alt="capturas" width="750"/>
<p>Una vez que se realizaron las configuraciones que se mostraron previamente, ahora si podremos acceder y hacer uso de esta herramienta para la gestion de los usuarios y grupos.</p>
<img src="./IMG/capturas/configuracion17.png" alt="capturas" width="900"/>
<p>Al momento de acceder el sistema detecta las unidades organizacionales (ou) que se especificaron previamente y nos pregunta si queremos crearlas, a lo cual le diremos que si y pulsamos el boton create.</p>

`NOTA: Antes de crear algun usuario primero se debe crear un grupo ya que, al crear el usuario este debe pertenecer a algun grupo.`

Para crear grupos basta con ir a la seccion "Groups", le damos en crear un grupo nuevo, colocamos el nombrel grupo y guardamos.

<img src="./IMG/capturas/configuracion18.png" alt="capturas" width="900"/>

Como parte de la gestion de grupos, se han creado dos grupos principales los cuales son: becarios y proferes. Y se han creado seis equipos secundarios, uno por cada area del proyecto.

Para crear a los usuarios debemos ir a la seccion de "Users" y seleccioner nuevos usuarios. Aqui a diferencia del apartado de grupos, se deben especificar mas detalles los cuales son los siguientes:

	* Identificador RDN
	* Last name

<img src="./IMG/capturas/configuracion19.png" alt="capturas" width="900"/>
<p>En la parte de UNIX</p>

	* user name
	* grupo primario
	* home directory
	* login shell

<img src="./IMG/capturas/configuracion20.png" alt="capturas" width="900"/>
<img src="./IMG/capturas/configuracion21.png" alt="capturas" width="600"/>
<img src="./IMG/capturas/configuracion22.png" alt="capturas" width="300"/>
<p>Y por ultimo se le crea un password en donde dice set password. A cada usuario se le asigno como password default el mismo nombre que tienen de usuario pero se le especifico como medida adicional que se fuerce a cambiar el password para que sean los usuarios quienes coloquen el que mas crean apropiados. Despues de colocar toda esta informacion se da en save para guardar el registro.</p>

<p>Para ver que los usuarios y grupos se han creado de forma correcta podemos acceder a la seccion donde dice tree, el cual nos muestra el arbol desde su raiz y tambien podemos usar el siguiente comando desde la terminal del sistema para confirmar que los usuarios que han sido creados se pueden ver</p>

	-getent passwd

<img src="./IMG/capturas/configuracion23.png" alt="capturas" width="900"/>
<p>Con todo esto ya podemos hacer gestion de todos los usuarios que requieran conectarse por LDAP asi como se pueden autenticar a traves de esto. </p>

Lo ultimo que falta es crear los home de nuestros usuarios para que puedan conectarse correctamente.

## Instalar el LDAP Tool Box Self Service Password

Para poder instalar el servicio se baso en el siguiente video:

[video de instalacion de LDAP Tool Box Self Service Password] https://www.youtube.com/watch?v=nAwzZlX806o
