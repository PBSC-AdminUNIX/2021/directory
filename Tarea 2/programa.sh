#!/bin/bash

echo "Por favor ingrese la ruta del archivo"
read -p ">>" ruta
echo "Su ruta es: "$ruta
while IFS='' read -r linea
do
	grupo_id=$(echo -e $linea | cut -f4 -d:)
	existe=$(cat /etc/group| cut -f3 -d:|egrep "$grupo_id" 2> error.log)
	if [ $existe ]
	then
		usuario_id=$(echo -e $linea | cut -f3 -d:)
		existe_usuario=$(cat /etc/passwd| cut -f3 -d:|egrep "$usuario_id" 2> error.log)
		if [ $existe_usuario ]
		then
			echo "El usuario ya existe en el sistema."			
		else		
			usuario=$(echo -e $linea | cut -f1 -d:)
			password=$(echo -e $linea | cut -f2 -d:)
			user_id=$(echo -e $linea | cut -f3 -d:)
			home=$(echo -e $linea | cut -f6 -d:)
			shell=$(echo -e $linea | cut -f7 -d:)
			sudo adduser $usuario -p $password -u $user_id -d $home -s $shell 2> error.log
			if [ $? == 0 ]
			then				
				echo "Usuario creado exitosamente."
			else
				echo "Error al crear el usuario."
			fi
		fi
	else
		echo "El grupo no existe."
	fi	
done < $ruta
